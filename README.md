# README #

### Group Members ###

* A20478906 ChengXin Zeng
* A20455340 Xin Zhao

### Team-A Phase I ER-model ###

* phase1.png
* This format can be opened for any image viewer
* Team A-phase II.jpg
* Change the ER-model according to the professor's suggestion

![picture](Team A-phase II.jpg)

### Team-A Phase II SQL script ###

* Team A-phase II.sql
* Team A-phaseII relational schema.png
* SQL script for PostgreSQL
* Derive a relational schema from the ER-MODEL and implement an SQL script that creates this schema for PostgreSQL. 
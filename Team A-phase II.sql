--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: online_grocery_store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA online_grocery_store;


ALTER SCHEMA online_grocery_store OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: address; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.address (
    address_id integer NOT NULL,
    street character varying,
    city character varying,
    state character varying
);


ALTER TABLE online_grocery_store.address OWNER TO postgres;

--
-- Name: address_address_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.address_address_id_seq OWNER TO postgres;

--
-- Name: address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.address_address_id_seq OWNED BY online_grocery_store.address.address_id;


--
-- Name: credit_card; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.credit_card (
    credit_card_no integer NOT NULL,
    address_id integer NOT NULL,
    vaild_thru date NOT NULL,
    cvv integer NOT NULL,
    customer_id integer NOT NULL
);


ALTER TABLE online_grocery_store.credit_card OWNER TO postgres;

--
-- Name: customer; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.customer (
    customer_id integer NOT NULL,
    customer_name character varying NOT NULL,
    password character varying NOT NULL,
    balance integer
);


ALTER TABLE online_grocery_store.customer OWNER TO postgres;

--
-- Name: customer_customer_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.customer_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.customer_customer_id_seq OWNER TO postgres;

--
-- Name: customer_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.customer_customer_id_seq OWNED BY online_grocery_store.customer.customer_id;


--
-- Name: image; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.image (
    image_id integer NOT NULL,
    image_name character varying NOT NULL,
    image_folder character varying NOT NULL,
    domain character varying NOT NULL
);


ALTER TABLE online_grocery_store.image OWNER TO postgres;

--
-- Name: image_image_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.image_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.image_image_id_seq OWNER TO postgres;

--
-- Name: image_image_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.image_image_id_seq OWNED BY online_grocery_store.image.image_id;


--
-- Name: order; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store."order" (
    order_id integer NOT NULL,
    order_date timestamp(0) without time zone NOT NULL,
    status character varying NOT NULL,
    customer_id integer NOT NULL,
    credit_card_no integer
);


ALTER TABLE online_grocery_store."order" OWNER TO postgres;

--
-- Name: order_item; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.order_item (
    order_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE online_grocery_store.order_item OWNER TO postgres;

--
-- Name: order_order_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.order_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.order_order_id_seq OWNER TO postgres;

--
-- Name: order_order_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.order_order_id_seq OWNED BY online_grocery_store."order".order_id;


--
-- Name: prodcut_price; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.prodcut_price (
    product_id integer NOT NULL,
    state character varying NOT NULL,
    price real NOT NULL
);


ALTER TABLE online_grocery_store.prodcut_price OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.product (
    product_id integer NOT NULL,
    product_name character varying NOT NULL,
    amount integer,
    category character varying,
    size real
);


ALTER TABLE online_grocery_store.product OWNER TO postgres;

--
-- Name: product_detail; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.product_detail (
    product_detail_id integer NOT NULL,
    type character varying NOT NULL,
    content character varying
);


ALTER TABLE online_grocery_store.product_detail OWNER TO postgres;

--
-- Name: product_detail_product_detail_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.product_detail_product_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.product_detail_product_detail_id_seq OWNER TO postgres;

--
-- Name: product_detail_product_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.product_detail_product_detail_id_seq OWNED BY online_grocery_store.product_detail.product_detail_id;


--
-- Name: product_image; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.product_image (
    product_id integer NOT NULL,
    image_id integer NOT NULL,
    priority integer
);


ALTER TABLE online_grocery_store.product_image OWNER TO postgres;

--
-- Name: product_product_detail; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.product_product_detail (
    product_id integer NOT NULL,
    product_detail_id integer NOT NULL
);


ALTER TABLE online_grocery_store.product_product_detail OWNER TO postgres;

--
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.product_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.product_product_id_seq OWNER TO postgres;

--
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.product_product_id_seq OWNED BY online_grocery_store.product.product_id;


--
-- Name: shopping_cart; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.shopping_cart (
    customer_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE online_grocery_store.shopping_cart OWNER TO postgres;

--
-- Name: staff; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.staff (
    staff_id integer NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    middle_name character varying,
    salary real,
    job_title character varying,
    address_id integer
);


ALTER TABLE online_grocery_store.staff OWNER TO postgres;

--
-- Name: stock; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.stock (
    warehouse_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE online_grocery_store.stock OWNER TO postgres;

--
-- Name: supplier; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.supplier (
    supplier_id integer NOT NULL,
    supplier_name character varying NOT NULL,
    address_id integer NOT NULL
);


ALTER TABLE online_grocery_store.supplier OWNER TO postgres;

--
-- Name: supplier_product; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.supplier_product (
    supplier_id integer NOT NULL,
    product_id integer NOT NULL,
    price real NOT NULL
);


ALTER TABLE online_grocery_store.supplier_product OWNER TO postgres;

--
-- Name: supplier_supplier_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.supplier_supplier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.supplier_supplier_id_seq OWNER TO postgres;

--
-- Name: supplier_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.supplier_supplier_id_seq OWNED BY online_grocery_store.supplier.supplier_id;


--
-- Name: warehouse; Type: TABLE; Schema: online_grocery_store; Owner: postgres
--

CREATE TABLE online_grocery_store.warehouse (
    warehouse_id integer NOT NULL,
    warehouse_name character varying NOT NULL,
    address_id integer NOT NULL,
    capacity real NOT NULL
);


ALTER TABLE online_grocery_store.warehouse OWNER TO postgres;

--
-- Name: warehouse_warehouse_id_seq; Type: SEQUENCE; Schema: online_grocery_store; Owner: postgres
--

CREATE SEQUENCE online_grocery_store.warehouse_warehouse_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE online_grocery_store.warehouse_warehouse_id_seq OWNER TO postgres;

--
-- Name: warehouse_warehouse_id_seq; Type: SEQUENCE OWNED BY; Schema: online_grocery_store; Owner: postgres
--

ALTER SEQUENCE online_grocery_store.warehouse_warehouse_id_seq OWNED BY online_grocery_store.warehouse.warehouse_id;


--
-- Name: address address_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.address ALTER COLUMN address_id SET DEFAULT nextval('online_grocery_store.address_address_id_seq'::regclass);


--
-- Name: customer customer_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.customer ALTER COLUMN customer_id SET DEFAULT nextval('online_grocery_store.customer_customer_id_seq'::regclass);


--
-- Name: image image_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.image ALTER COLUMN image_id SET DEFAULT nextval('online_grocery_store.image_image_id_seq'::regclass);


--
-- Name: order order_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store."order" ALTER COLUMN order_id SET DEFAULT nextval('online_grocery_store.order_order_id_seq'::regclass);


--
-- Name: product product_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product ALTER COLUMN product_id SET DEFAULT nextval('online_grocery_store.product_product_id_seq'::regclass);


--
-- Name: product_detail product_detail_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_detail ALTER COLUMN product_detail_id SET DEFAULT nextval('online_grocery_store.product_detail_product_detail_id_seq'::regclass);


--
-- Name: supplier supplier_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.supplier ALTER COLUMN supplier_id SET DEFAULT nextval('online_grocery_store.supplier_supplier_id_seq'::regclass);


--
-- Name: warehouse warehouse_id; Type: DEFAULT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.warehouse ALTER COLUMN warehouse_id SET DEFAULT nextval('online_grocery_store.warehouse_warehouse_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.address (address_id, street, city, state) FROM stdin;
\.


--
-- Data for Name: credit_card; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.credit_card (credit_card_no, address_id, vaild_thru, cvv, customer_id) FROM stdin;
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.customer (customer_id, customer_name, password, balance) FROM stdin;
\.


--
-- Data for Name: image; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.image (image_id, image_name, image_folder, domain) FROM stdin;
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store."order" (order_id, order_date, status, customer_id, credit_card_no) FROM stdin;
\.


--
-- Data for Name: order_item; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.order_item (order_id, product_id, quantity) FROM stdin;
\.


--
-- Data for Name: prodcut_price; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.prodcut_price (product_id, state, price) FROM stdin;
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.product (product_id, product_name, amount, category, size) FROM stdin;
\.


--
-- Data for Name: product_detail; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.product_detail (product_detail_id, type, content) FROM stdin;
\.


--
-- Data for Name: product_image; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.product_image (product_id, image_id, priority) FROM stdin;
\.


--
-- Data for Name: product_product_detail; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.product_product_detail (product_id, product_detail_id) FROM stdin;
\.


--
-- Data for Name: shopping_cart; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.shopping_cart (customer_id, product_id, quantity) FROM stdin;
\.


--
-- Data for Name: staff; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.staff (staff_id, first_name, last_name, middle_name, salary, job_title, address_id) FROM stdin;
\.


--
-- Data for Name: stock; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.stock (warehouse_id, product_id, quantity) FROM stdin;
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.supplier (supplier_id, supplier_name, address_id) FROM stdin;
\.


--
-- Data for Name: supplier_product; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.supplier_product (supplier_id, product_id, price) FROM stdin;
\.


--
-- Data for Name: warehouse; Type: TABLE DATA; Schema: online_grocery_store; Owner: postgres
--

COPY online_grocery_store.warehouse (warehouse_id, warehouse_name, address_id, capacity) FROM stdin;
\.


--
-- Name: address_address_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.address_address_id_seq', 1, false);


--
-- Name: customer_customer_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.customer_customer_id_seq', 1, false);


--
-- Name: image_image_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.image_image_id_seq', 1, false);


--
-- Name: order_order_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.order_order_id_seq', 1, false);


--
-- Name: product_detail_product_detail_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.product_detail_product_detail_id_seq', 1, false);


--
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.product_product_id_seq', 1, false);


--
-- Name: supplier_supplier_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.supplier_supplier_id_seq', 1, false);


--
-- Name: warehouse_warehouse_id_seq; Type: SEQUENCE SET; Schema: online_grocery_store; Owner: postgres
--

SELECT pg_catalog.setval('online_grocery_store.warehouse_warehouse_id_seq', 1, false);


--
-- Name: address address_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.address
    ADD CONSTRAINT address_pk PRIMARY KEY (address_id);


--
-- Name: credit_card credit_card_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.credit_card
    ADD CONSTRAINT credit_card_pk PRIMARY KEY (credit_card_no);


--
-- Name: customer customer_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.customer
    ADD CONSTRAINT customer_pk PRIMARY KEY (customer_id);


--
-- Name: image image_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.image
    ADD CONSTRAINT image_pk PRIMARY KEY (image_id);


--
-- Name: order_item order_item_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.order_item
    ADD CONSTRAINT order_item_pk PRIMARY KEY (order_id, product_id);


--
-- Name: order order_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store."order"
    ADD CONSTRAINT order_pk PRIMARY KEY (order_id);


--
-- Name: prodcut_price prodcut_price_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.prodcut_price
    ADD CONSTRAINT prodcut_price_pk PRIMARY KEY (product_id, state);


--
-- Name: product_detail product_detail_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_detail
    ADD CONSTRAINT product_detail_pk PRIMARY KEY (product_detail_id);


--
-- Name: product_image product_image_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_image
    ADD CONSTRAINT product_image_pk PRIMARY KEY (product_id, image_id);


--
-- Name: product product_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product
    ADD CONSTRAINT product_pk PRIMARY KEY (product_id);


--
-- Name: product_product_detail product_product_detail_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_product_detail
    ADD CONSTRAINT product_product_detail_pk PRIMARY KEY (product_id, product_detail_id);


--
-- Name: shopping_cart shopping_cart_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.shopping_cart
    ADD CONSTRAINT shopping_cart_pk PRIMARY KEY (customer_id, product_id);


--
-- Name: staff staff_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.staff
    ADD CONSTRAINT staff_pk PRIMARY KEY (staff_id);


--
-- Name: supplier supplier_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.supplier
    ADD CONSTRAINT supplier_pk PRIMARY KEY (supplier_id);


--
-- Name: supplier_product supplier_product_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.supplier_product
    ADD CONSTRAINT supplier_product_pk PRIMARY KEY (supplier_id, product_id);


--
-- Name: warehouse warehouse_pk; Type: CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.warehouse
    ADD CONSTRAINT warehouse_pk PRIMARY KEY (warehouse_id);


--
-- Name: credit_card credit_card_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.credit_card
    ADD CONSTRAINT credit_card_fk FOREIGN KEY (customer_id) REFERENCES online_grocery_store.customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: credit_card credit_card_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.credit_card
    ADD CONSTRAINT credit_card_fk_1 FOREIGN KEY (address_id) REFERENCES online_grocery_store.address(address_id);


--
-- Name: order order_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store."order"
    ADD CONSTRAINT order_fk FOREIGN KEY (customer_id) REFERENCES online_grocery_store.customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: order order_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store."order"
    ADD CONSTRAINT order_fk_1 FOREIGN KEY (credit_card_no) REFERENCES online_grocery_store.credit_card(credit_card_no);


--
-- Name: order_item order_item_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.order_item
    ADD CONSTRAINT order_item_fk FOREIGN KEY (order_id) REFERENCES online_grocery_store."order"(order_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: order_item order_item_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.order_item
    ADD CONSTRAINT order_item_fk_1 FOREIGN KEY (product_id) REFERENCES online_grocery_store.product(product_id);


--
-- Name: prodcut_price prodcut_price_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.prodcut_price
    ADD CONSTRAINT prodcut_price_fk FOREIGN KEY (product_id) REFERENCES online_grocery_store.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_image product_image_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_image
    ADD CONSTRAINT product_image_fk FOREIGN KEY (product_id) REFERENCES online_grocery_store.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_image product_image_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_image
    ADD CONSTRAINT product_image_fk_1 FOREIGN KEY (image_id) REFERENCES online_grocery_store.image(image_id);


--
-- Name: product_product_detail product_product_detail_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_product_detail
    ADD CONSTRAINT product_product_detail_fk FOREIGN KEY (product_detail_id) REFERENCES online_grocery_store.product_detail(product_detail_id);


--
-- Name: product_product_detail product_product_detail_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.product_product_detail
    ADD CONSTRAINT product_product_detail_fk_1 FOREIGN KEY (product_id) REFERENCES online_grocery_store.product(product_id);


--
-- Name: shopping_cart shopping_cart_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.shopping_cart
    ADD CONSTRAINT shopping_cart_fk FOREIGN KEY (customer_id) REFERENCES online_grocery_store.customer(customer_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: staff staff_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.staff
    ADD CONSTRAINT staff_fk FOREIGN KEY (address_id) REFERENCES online_grocery_store.address(address_id);


--
-- Name: stock stock_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.stock
    ADD CONSTRAINT stock_fk FOREIGN KEY (warehouse_id) REFERENCES online_grocery_store.warehouse(warehouse_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stock stock_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.stock
    ADD CONSTRAINT stock_fk_1 FOREIGN KEY (product_id) REFERENCES online_grocery_store.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supplier supplier_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.supplier
    ADD CONSTRAINT supplier_fk FOREIGN KEY (address_id) REFERENCES online_grocery_store.address(address_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supplier_product supplier_product_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.supplier_product
    ADD CONSTRAINT supplier_product_fk FOREIGN KEY (supplier_id) REFERENCES online_grocery_store.supplier(supplier_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supplier_product supplier_product_fk_1; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.supplier_product
    ADD CONSTRAINT supplier_product_fk_1 FOREIGN KEY (product_id) REFERENCES online_grocery_store.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse warehouse_fk; Type: FK CONSTRAINT; Schema: online_grocery_store; Owner: postgres
--

ALTER TABLE ONLY online_grocery_store.warehouse
    ADD CONSTRAINT warehouse_fk FOREIGN KEY (address_id) REFERENCES online_grocery_store.address(address_id);


--
-- PostgreSQL database dump complete
--

